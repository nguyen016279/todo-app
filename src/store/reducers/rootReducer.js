import { combineReducers } from "redux";
import todosReducer from "./todoReducers";

export default combineReducers({
  todos: todosReducer,
});
