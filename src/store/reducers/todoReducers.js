import {
  ADD_TODO,
  GET_TODO_EDITING_ID,
  ON_EDIT_TODO,
  MARK_COMPLETED,
  CHECK_ALL_TODOS,
  REMOVE_TODO,
  SET_STATUS_FILTER,
  CLEAR_COMPLETED,
  SEARCH_TODO,
} from "../actions";
import { isNotCheckedAll, filterByStatus } from "../../helpers";

if (localStorage.getItem("list") === null) {
  localStorage.setItem(
    "list",
    JSON.stringify({
      todosList: [
        {
          id: 1,
          text: "todo 1",
          isCompleted: false,
        },
        {
          id: 2,
          text: "todo 2",
          isCompleted: false,
        },
      ],
      todoEditingId: "",
      isCheckedAll: false,
      status: "ALL",
    })
  );
}

const todosReducers = (
  state = JSON.parse(localStorage.getItem("list")),
  action
) => {
  const { todosList, isCheckedAll } = state;
  const list = JSON.parse(JSON.stringify(todosList));
  switch (action.type) {
    case ADD_TODO:
      localStorage.setItem(
        "list",
        JSON.stringify(
          Object.assign({}, state, {
            todosList: [...list, action.todo],
          })
        )
      );
      return Object.assign({}, state, {
        todosList: [...list, action.todo],
      });
    case GET_TODO_EDITING_ID:
      localStorage.setItem(
        "list",
        JSON.stringify(
          Object.assign({}, state, {
            todoEditingId: action.id,
          })
        )
      );
      return Object.assign({}, state, {
        todoEditingId: action.id,
      });
    case ON_EDIT_TODO:
      if (action.index >= 0) {
        list.splice(action.index, 1, action.todo);
      }
      localStorage.setItem(
        "list",
        JSON.stringify(
          Object.assign({}, state, {
            todosList: list,
            todoEditingId: "",
          })
        )
      );
      return Object.assign({}, state, {
        todosList: list,
        todoEditingId: "",
      });
    case MARK_COMPLETED:
      const updatedList = todosList.map((todo) =>
        todo.id === action.id
          ? { ...todo, isCompleted: !todo.isCompleted }
          : todo
      );
      localStorage.setItem(
        "list",
        JSON.stringify(
          Object.assign({}, state, {
            todosList: updatedList,
            isCheckedAll: !isNotCheckedAll(updatedList),
          })
        )
      );
      return Object.assign({}, state, {
        todosList: updatedList,
        isCheckedAll: !isNotCheckedAll(updatedList),
      });
    case CHECK_ALL_TODOS:
      localStorage.setItem(
        "list",
        JSON.stringify(
          Object.assign({}, state, {
            todosList: todosList.map((todo) => ({
              ...todo,
              isCompleted: !isCheckedAll,
            })),
            isCheckedAll: !isCheckedAll,
          })
        )
      );
      return Object.assign({}, state, {
        todosList: todosList.map((todo) => ({
          ...todo,
          isCompleted: !isCheckedAll,
        })),
        isCheckedAll: !isCheckedAll,
      });
    case REMOVE_TODO:
      localStorage.setItem(
        "list",
        JSON.stringify(
          Object.assign({}, state, {
            todosList: filterByStatus(todosList, "REMOVE", action.id),
          })
        )
      );
      return Object.assign({}, state, {
        todosList: filterByStatus(todosList, "REMOVE", action.id),
      });
    case SET_STATUS_FILTER:
      localStorage.setItem(
        "list",
        JSON.stringify(
          Object.assign({}, state, {
            status: action.status,
          })
        )
      );
      return Object.assign({}, state, {
        status: action.status,
      });
    case CLEAR_COMPLETED:
      localStorage.setItem(
        "list",
        JSON.stringify(
          Object.assign({}, state, {
            todosList: filterByStatus(todosList, "ACTIVE"),
          })
        )
      );
      return Object.assign({}, state, {
        todosList: filterByStatus(todosList, "ACTIVE"),
      });
    case SEARCH_TODO:
      return Object.assign({}, state, {
        todosList: state.todosList.filter((item) =>
          item.text.includes(action.todoName)
        ),
      });
    default:
      return state;
  }
};

export default todosReducers;
