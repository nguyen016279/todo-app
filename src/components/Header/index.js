import React, { memo, useState } from "react";
import { connect } from "react-redux";
import { filterByStatus } from "../../helpers";
import { addTodo } from "../../store/actions";

const Header = memo((props) => {
  const [text, setText] = useState("");
  const { addTodo, isCheckedAll, todosList } = props;
  const onAddTodo = (e = {}) => {
    if (e.key === "Enter" && text) {
      todosList.filter((item) => item.text === text).length > 0
        ? alert("this task is exist")
        : addTodo({
            id: new Date().valueOf(),
            text,
            isCompleted: false,
          });
      setText("");
    }
  };
  return (
    <header className="header">
      <h1>todos</h1>
      <input
        className="new-todo"
        value={text}
        onChange={(e) => setText(e.target.value)}
        onKeyPress={(e) => onAddTodo(e)}
        checked={isCheckedAll}
      />
    </header>
  );
});

const mapStateToProps = (state) => {
  return {
    isCheckedAll: state.todos.isCheckedAll,
    todosList: filterByStatus(state.todos.todosList, state.todos.status),
  };
};

const mapDispatchToProps = {
  addTodo,
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
