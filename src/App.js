import React from "react";
import { Provider } from "react-redux";
import store from "./store";

// Components
import Header from "./components/Header";
import TodoList from "./components/TodoList";
import Footer from "./components/Footer";

// Css
import "./App.css";
import 'antd/dist/antd.css';

function App() {
  return (
    <Provider store={store}>
      <div className="todoapp">
        <Header />
        <TodoList />
        <Footer />
      </div>
    </Provider>
  );
}

export default App;
